export type TCallable = {
  new (...args: unknown[]): unknown
}
