import { container } from "./container"

export const Inject =
  (target: Function) => (_target: Object, _propertyKey: string, _parameterIndex?: number) => {
    container.getInstance(target?.name)
  }
