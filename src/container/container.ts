import { TCallable } from "./types"

type TDepMeta = {
  deps: string[]
  target: TCallable
  instance?: Object
}

class Container {
  private depList = new Map<string, TDepMeta>()

  public register(target: TCallable): void {
    const propsTypes = Reflect.getMetadata("design:paramtypes", target)
    const deps: string[] = propsTypes?.map((props: TCallable) => props.name)
    this.depList.set(target.name, { deps, target })
  }

  public getInstance(className: string): Object {
    const meta = this.getMeta(className)
    if (meta.instance) {
      return meta.instance
    }

    const depsInstances: Object[] = []
    if (meta.deps?.length) {
      meta.deps.forEach((item) => {
        const dependency = this.getInstance(item)
        depsInstances.push(dependency)
      })
    }

    meta.instance = new meta.target(...depsInstances)
    return meta.instance
  }

  private getMeta(className: string): TDepMeta {
    const meta = this.depList.get(className)
    if (!meta) {
      throw new Error("Class not found")
    }
    return meta
  }
}

export const container = new Container()
