import { container } from "./container"
import { TCallable } from "./types"

export const Injectable =
  <T extends TCallable>() =>
  (target: T) => {
    container.register(target)
  }
