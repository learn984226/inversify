import { Inject, Injectable } from "../../container"
import { Rubber } from "../materials"

@Injectable()
export class Wheel {
  constructor(@Inject(Rubber) private readonly rubber: Rubber) {}

  public getRubber(): Rubber {
    return this.rubber
  }
}
