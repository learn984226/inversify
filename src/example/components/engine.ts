import { Inject, Injectable } from "../../container"
import { Steel } from "../materials"

@Injectable()
export class Engine {
  constructor(@Inject(Steel) private readonly steel: Steel) {}

  public getSteel(): Steel {
    return this.steel
  }
}
