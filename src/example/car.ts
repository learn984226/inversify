import { Inject, Injectable } from "../container"
import { Engine, Wheel } from "./components"

@Injectable()
export class Car {
  constructor(
    @Inject(Engine) private readonly engine: Engine,
    @Inject(Wheel) private readonly wheel: Wheel
  ) {}

  public getEngine(): Engine {
    return this.engine
  }

  public getWheel(): Wheel {
    return this.wheel
  }
}
