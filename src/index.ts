import "reflect-metadata"
import { container } from "./container"
import { Car } from "./example"

const car = container.getInstance(Car.name)
console.log(car)
